<?php
namespace Bnl\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Bnl\Factory\AbstractFactory;

class ModelFactory extends AbstractFactory
{
    public static $modelNamespace;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {        
        $adapter = $serviceLocator->get('Zend\Db\Adapter');        
        $config = $serviceLocator->get('Config');
        
        $modelNamespace = end(self::$modelNamespaces);        
        $entity = $config['models'][$modelNamespace];  
       
        $model = new $modelNamespace($entity['keyName'],
        		  $entity['tableName'],
        		  $adapter
        );
        
        $model->setServiceManager($serviceLocator);
        self::popLastNamespace();
        
        return $model;
    }
}

?>