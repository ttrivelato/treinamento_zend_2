<?php
namespace Bnl\Factory;

use Zend\ServiceManager\FactoryInterface;

/**
 * TODO
 * @author fgsl
 *
 */
abstract class AbstractFactory implements FactoryInterface
{
    protected static $modelNamespaces = array();
    
    public static function addModelNamespace(
        $modelNamespace)
    {
    	self::$modelNamespaces[] = $modelNamespace;
    }

    public static function popLastNamespace()
    {
        $last = end(self::$modelNamespaces);
        unset($last);
    }
}
?>