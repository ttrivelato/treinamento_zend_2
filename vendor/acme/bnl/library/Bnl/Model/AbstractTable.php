<?php
namespace Bnl\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceManager;
use Zend\Db\Sql\Select;
abstract class AbstractTable
{
    /**
     * 
     * @var TableGateway
     */
    protected $tableGateway;
    
    /**
     * 
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * 
     * @var AbstractModel
     */
    protected $modelInstance = null;
    
    
    public function __construct(TableGateway $tableGateway,
                                  $modelInstance,    
                                  ServiceManager $serviceManager = null)
    {
        $this->tableGateway = $tableGateway;
        $this->modelInstance = $modelInstance;
        $this->serviceManager = $serviceManager;
    }
    
   public function fetchAll($where = null)
   {
   		if($where instanceof Select) 
   		{
   			return $this->tableGateway->selectWith($where);
   		}
   		
   		return $this->tableGateway->select();   	
   }
   
   public function get($key)
   {
       $resultSet = $this->tableGateway->select(array(
       	    $this->modelInstance->getKeyName() => $key
       ));
       
       if ($resultSet->count() == 0)
       {
           return $this->modelInstance;
       }
       return $resultSet->current();
   }
   
   public function delete($key)
   {
       $this->tableGateway->delete(array(
       	    $this->modelInstance->getKeyName() => $key
       ));
   }
   
   public function getKeyName()
   {
       return $this->modelInstance->getKeyName();
   }
    
    
    
    
}

?>