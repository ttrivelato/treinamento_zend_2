<?php
namespace Bnl\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\Filter\FilterChain;
use Zend\Validator\ValidatorChain;
use Zend\ServiceManager\ServiceManager;
use Zend\Db\RowGateway\RowGateway;

abstract class AbstractModel extends RowGateway
{    
    protected $modelInputs = array();

    protected $inputFilter;
    
    protected $serviceManager = null;
    
    public function getInputFilter()
    {
        if (! $this->inputFilter) {
            $inputFilter = new InputFilter();
            foreach ($this->modelInputs as $name => $modelInput) {
                $input = new Input($name);
                
                $filterChain = new FilterChain();
                
                $modelInput['filters'] = isset($modelInput['filters']) 
                ? $modelInput['filters'] : array(); 
                foreach ($modelInput['filters'] as $filter) {
                    $filterChain->attach($filter);
                }
                $input->setFilterChain($filterChain);
                
                $validatorChain = new ValidatorChain();
                
                $modelInput['validators'] = isset($modelInput['validators'])
                ? $modelInput['validators'] : array();                
                foreach ($modelInput['validators'] as $validator) {
                    $validatorChain->attach($validator);
                }
                $input->setValidatorChain($validatorChain);
                
                $modelInput['properties'] = isset($modelInput['properties'])
                ? $modelInput['properties'] : array();
                foreach($modelInput['properties'] as $method => $value)
                {
                    $input->$method($value);
                }                
                
                $inputFilter->add($input);
            }
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    public function getArrayCopy()
    {        
        return $this->data;
    }
    
    public function populate($rowData)
    {
        $rowData = $this->cleanData($rowData);       
        parent::populate($rowData);
    }
    
    protected function cleanData($data)
    {
        $arrayCopy = array();
        foreach($this->modelInputs as $input => $value)
        {
        	$arrayCopy[$input] = $data[$input];
        }
        return $arrayCopy;
    }
    
    public function exchangeArray($array)
    {
        parent::exchangeArray($array);
        $keyData = $array[$this->getKeyName()];
        if (!empty($keyData))
        {
            $this->primaryKeyData = array(
        		$this->getKeyName() => $array[$this->getKeyName()]
            );
        }        
    }
    
    public function getKeyName()
    {
        return implode('',$this->primaryKeyColumn);
    }
    
    public function getTable()
    {
        return $this->table;
    }
    
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }
}










?>