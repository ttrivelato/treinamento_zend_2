<?php
namespace Bnl\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Bnl\Model\AbstractTable;
use Zend\View\Model\ViewModel;
use Zend\Form\Form;
use Bnl\Model\AbstractModel;
use Bnl\Factory\TableFactory;
use Bnl\Factory\ModelFactory;

class AbstractController extends AbstractActionController
{
    protected $formName = null;
    protected $modelName = null;
    protected $routeName = null;   
    

    /**
     * The default action - show the home page
     */
    public function indexAction()
    {
    	// TODO Auto-generated CategoriaController::indexAction() default action
    	$table = $this->getTable();
    	$resultSet = $table->fetchAll();
    	return new ViewModel(array(
    			'resultSet' => $resultSet
    	));
    }
    
    public function editAction()
    {
    	if (isset($_SESSION['form']))
    	{
    		$form = $_SESSION['form'];
    		unset($_SESSION['form']);
    	}
    	else
    	{
    	    $formName = $this->formName;
    		$form = new $formName();
    		$this->setForm($form);
    		
    		$key = $this->params('key', null);

    		$model = $this->getModel($this->modelName);

    		if (! empty($key)) {
    			$table = $this->getTable();
    			$model = $table->get($key);
    		}
    
    		$form->bind($model);
    	}
    	$form->setAttribute('action', $this->url()
    			->fromRoute($this->routeName, array(
    					'controller' => $this->params('controller'), 
    					'action' => 'save'
    			)));
    
    	return array(
    			'form' => $form
    	);
    }
    
    public function saveAction()
    {
    	$table = $this->getTable();
    	$model = $table->get($this->getRequest()->getPost($table->getKeyName()));
            
    	$formName = $this->formName;
    	$form = new $formName();
    	$form->setInputFilter($model->getInputFilter());
    	$this->setForm($form);
    	$form->bind($this->getRequest()
    			->getPost());
    
    	if (! $form->isValid()) {
    		$_SESSION['form'] = $form;
    		return $this->redirect()->toRoute($this->routeName, array(
    				'controller' => $this->params('controller'),
    				'action' => 'edit'
    		));
    	}    	
    
    	$model->exchangeArray($form->getData(Form::VALUES_AS_ARRAY));
    
       	$model->save();
    
    	$this->redirect()->toRoute($this->routeName, array(
    			'controller' => $this->params('controller')
    	));
    }
    
    public function deleteAction()
    {
    	$key = $this->params('key', null);
    
    	$this->getTable()->delete($key);
    
    	$this->redirect()->toRoute($this->routeName, array(
    			'controller' => $this->params('controller')
    	));
    }
    
    /**
     *
     * @return AbstractTable
     */
    protected function getTable($name = null)
    {
        $name = is_null($name) ? $this->modelName : $name;
        TableFactory::addModelNamespace($name);
    	return $this->getServiceLocator()->create('TableGateways');
    }    
    
    protected function setForm($form)
    {        
    }
    
    /**
     * 
     * @param string $modelName
     * @return AbstractModel
     */
    protected function getModel($modelName)
    {
        ModelFactory::addModelNamespace($modelName);
        return $this->getServiceLocator()
        ->create('Models');        
    }
    
    
    
    
    
    
    
}

?>