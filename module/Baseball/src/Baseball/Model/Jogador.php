<?php
namespace Baseball\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\Filter\FilterChain;
use Zend\Filter\StringToUpper;
use Zend\Filter\StripNewlines;
use Zend\Filter\StripTags;
use Zend\Validator\ValidatorChain;
use Zend\Validator\StringLength;
use Zend\I18n\Validator\Alnum;
use Zend\I18n\Validator\Int;
use Zend\ServiceManager\ServiceManager;
use Bnl\Model\AbstractModel;
use Zend\Validator\Digits;
use Zend\Db\RowGateway\RowGateway;
use Bnl\Factory\TableFactory;
class Jogador extends AbstractModel
{
    protected $keyName = 'inscricao';
    
    public function __construct($primaryKeyColumn,$table,$adapter)
    {
        parent::__construct($primaryKeyColumn, $table, $adapter);
        $this->categoria = new Categoria('codigo',
                                          'categorias',
                                          $adapter);
        $this->modelInputs = array(
        	'inscricao' => array(
        	   'properties' => array(
        		  'setAllowEmpty' => true
        	   )
        	),
            'nome' => array(
        		'filters' => array(
            	   new StringToUpper(),
        		   new StripTags() 
                ),
                'validators' => array(
        			new StringLength(
                	   array('min'=>3,'max'=>30)
                    ),
                    new Alnum()
        		)
        	),
            'codigo_categoria' => array(
            	'validators' => array(
            	   new Digits()
                )
            )
        );
        
    }

    public function exchangeArray($array)
    {
        parent::exchangeArray($array);
        TableFactory::addModelNamespace('Baseball\Model\Categoria');
        $tableCategoria = $this->serviceManager->create('TableGateways');
        
        $this->data['categoria'] = $tableCategoria->get($array['codigo_categoria']);
    }    
  
    public function getArrayCopy()
    {
        unset($this->data['categoria']);
        return $this->data;
    }
    
    public function save()
    {
        unset($this->data['categoria']);
        parent::save();
    }
    
    
    
    
    
}

?>