<?php
namespace Baseball\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\Filter\FilterChain;
use Zend\Filter\StringToUpper;
use Zend\Filter\StripNewlines;
use Zend\Filter\StripTags;
use Zend\Validator\ValidatorChain;
use Zend\Validator\StringLength;
use Zend\I18n\Validator\Alnum;
use Bnl\Model\AbstractModel;
use Zend\Db\RowGateway\RowGateway;
class Categoria extends AbstractModel
{
    protected $keyName = 'codigo';
    
    public function __construct($primaryKeyColumn,$table,$adapter)
    {
        parent::__construct($primaryKeyColumn, $table, $adapter);
        $this->modelInputs = array(
        	'codigo' => array(
        	    'properties' => array(
        		      'setAllowEmpty' => true 
        	)
        	 ),
            'descricao' => array(
        		'filters' => array(
            	   new StringToUpper('UTF-8'),
        		   new StripTags() 
                ),
                'validators' => array(
        			new StringLength(
                	   array('min'=>3,'max'=>30)
                    ),
                    new Alnum()
        		)
        	)
        );
    }
   
    
    
}

?>