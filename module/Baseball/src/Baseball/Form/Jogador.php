<?php
namespace Baseball\Form;

use Zend\Form\Form;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Text;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Select;
use Zend\Db\ResultSet\ResultSet;

class Jogador extends Form
{
    public function __construct()
    {
        parent::__construct('jogador');

        $this->setAttribute('method', 'post');
        
        $elementOrFieldset = new Hidden('inscricao');
        
        $this->add($elementOrFieldset);
        
        $elementOrFieldset = new Text('nome');
        $elementOrFieldset->setLabel('Nome:');
        $elementOrFieldset->setAttribute('autofocus', 'autofocus');
        
        $this->add($elementOrFieldset);        
        
        $elementOrFieldset = new Select('codigo_categoria');
        $elementOrFieldset->setLabel('Categoria:');
        
        $this->add($elementOrFieldset);        
        
        $elementOrFieldset = new Submit('gravar');
        $elementOrFieldset->setValue('gravar');
        
        $this->add($elementOrFieldset);       
    }
    
    public function setCategorias(ResultSet $categorias)
    {
        $options = array();
        foreach($categorias as $categoria)
        {
            $options[$categoria->codigo] = $categoria->descricao;
        }
                
        $this->get('codigo_categoria')->setValueOptions($options); 
    }
    
    
    
    
    
    
    
    
    
    
}

?>