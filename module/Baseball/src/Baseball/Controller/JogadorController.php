<?php
namespace Baseball\Controller;

use Bnl\Controller\AbstractController;

/**
 * JogadorController
 *
 * @author
 *
 *
 * @version
 *
 *
 */
class JogadorController extends AbstractController
{
    public function __construct()
    {
    	$this->formName = 'Baseball\Form\Jogador';
    	$this->modelName = 'Baseball\Model\Jogador';
    	$this->routeName = 'baseball';
    }    
    
    public function setForm($form)
    {
        $categorias = $this->getTable('Baseball\Model\Categoria')->fetchAll();
        $form->setCategorias($categorias);
    }
    
    
    
    
}