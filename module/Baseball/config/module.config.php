<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'index' => 'Baseball\Controller\IndexController',
            'jogador' => 'Baseball\Controller\JogadorController',
            'categoria' => 'Baseball\Controller\CategoriaController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'baseball' => array(
                'type'    => 'Segment',
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/baseball[/[:controller[/:action[/:key]]]]',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        'controller'    => 'index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    // This route is a sane default when developing a module;
                    // as you solidify the routes for your module, however,
                    // you may want to remove it and replace it with more
                    // specific routes.
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Baseball' => __DIR__ . '/../view',
        		
        ),
    	'template_map' => array(
    		'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',    				
    	),
    ),
    'models' => array(
        	'Baseball\Model\Categoria' => array(
        	   'keyName' => 'codigo',
        	   'tableName' => 'categorias' 
            ),
            'Baseball\Model\Jogador' => array(
        		'keyName' => 'inscricao',
        		'tableName' => 'jogadores'
            ),        
        ),  
    
);
