<?php

namespace Application\Model;

use Bnl\Model\AbstractModel;
use Zend\Permissions\Acl\Acl;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Bnl\Factory\TableFactory;

class Usuario extends AbstractModel {
	
	/**
	 * 
	 * @var Adapter
	 */
	private $adapter;
	
	public function __construct($primaryKeyColumn, $table, $adapter)
	{
		$this->adapter = $adapter;
		parent::__construct($primaryKeyColumn, $table, $adapter);
	}
	
	public static function getAcl($nomeUsuario, $serviceManager)
	{
		$acl = new Acl();
		$adapter = $serviceManager->get('Zend\Db\Adapter');
		
		$select = new Select('usuarios');
		$select->join('papeis_usuario', 'usuarios.codigo = papeis_usuario.codigo_usuario');
		
		$select->where(array('usuarios.nome' =>$nomeUsuario));
		
		$sql = $select->getSqlString($adapter->getPlatform());

		$statement = $adapter->query($sql);
		$resultSet = $statement->execute();
		
		foreach($resultSet as $row)
		{
			$acl->addRole($row['codigo_papel']);
		}
		
		$select = new Select('permissoes');
		$sql = $select->getSqlString($adapter->getPlatform());
		
		$statement = $adapter->query($sql);
		$resultSet = $statement->execute();
		
		foreach($resultSet as $row) 
		{
			$acl->addResource($row['permissao']);
			if($acl->hasRole($row['codigo_papel']))
			$acl->allow($row['codigo_papel'], $row['permisao']);
		}
		
		return $acl;		
	}	
}