<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element\Text;
use Zend\Form\Element\Password;
use Zend\Form\Element\Submit;

class Login extends Form {
	
	function __construct()
	{
		parent::__construct($name);
		
		$this->setAttribute('method', 'post');
		
		$elementOrFieldset = new Text('usuario');
		$elementOrFieldset->setLabel('Usuario');
		$elementOrFieldset->setAttribute('autofocus', 'autofocus');
		$this->add($elementOrFieldset);
		
		$elementOrFieldset = new Password('senha');
		$elementOrFieldset->setLabel('Senha');		
		$this->add($elementOrFieldset);
		
		$elementOrFieldset = new Submit('entrar');
		$elementOrFieldset->setValue('Entrar');
		$this->add($elementOrFieldset);
	}
}
?>