<?php

namespace Application\Service;

use Zend\Mvc\MvcEvent;
use Zend\Authentication\AuthenticationService;
class Acl {
	
	public function checkPermissions(MvcEvent $event)
	{
		$application = $event->getApplication();
		$request = $application->getRequest();
		$uri = $request->getRequestUri();
		$tokens = explode('/',$uri);
				
		if(in_array('jogador', $tokens) || in_array('categoria', $tokens))
		{
			$resource = in_array('jogador', $tokens) ? 'jogador' : 'categoria';
		} else {
			return;
		}
		
		$authentication = new AuthenticationService();				
		if(!$authentication->hasIdentity())
		{
			return;
		}
		
		$usuario = $authentication->getStorage()->read();
		$acl = $usuario['acl'];
		
		foreach($acl->getRoles() as $role) {
		
			if(!$acl->isAllowed($role, $resource))
			{
				header('Location: '.$request->getBaseUrl()."/baseball");
				exit;
			}		
		}
	}
}

?>