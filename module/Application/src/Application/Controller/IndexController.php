<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Form\Login;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable;
use Application\Model\Usuario;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
    	$form = new Login();
    	$form->setAttribute('action', $this->url()->fromRoute('application', array('action'=>'login')));
    	
    	$message = implode(",",$this->flashmessenger()->getMessages());
    	$this->flashmessenger()->clearMessages();
    	
        return new ViewModel(array('form'=>$form, 'message'=> $message));
    }
    
    public function loginAction()
    {
    	$usuario = $this->getRequest()->getPost('usuario');
    	$senha = $this->getRequest()->getPost('senha');
    	
    	$autentication = new AuthenticationService();
    	
    	$zendDb = $this->getServiceLocator()->get('Zend\Db\Adapter');
    	$adapter = new DbTable($zendDb);
    	$adapter->setTableName('usuarios');
    	$adapter->setIdentityColumn('nome');
    	$adapter->setCredentialColumn('senha');
    	$adapter->setIdentity($usuario);
    	$adapter->setCredential(md5($senha));
    	
    	$autentication->setAdapter($adapter);
    	
    	$result = $autentication->authenticate();
    	
    	if($result->isValid())
    	{  		
    		$acl = Usuario::getAcl($usuario, $this->getServiceLocator());
    		$autentication->getStorage()->write(array('nome' => $usuario, 'acl'=>$acl));
    		
    		return $this->redirect()->toRoute('baseball');
    	}

    	$this->flashmessenger()->addMessage(implode(",",$result->getMessages()));
    	return $this->redirect()->toRoute('application');    	
    }
    
    public function logoutAction()
    {
    	$autentication = new AuthenticationService();
    	$autentication->clearIdentity();
    	
    	return $this->redirect()->toRoute('application');
    }
}
