<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Session\SessionManager;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\Mvc\I18n\Translator;
use Zend\Validator\AbstractValidator;
use Bnl\Service\Authentication;
use Application\Service\Acl;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
                
        $translator = new Translator();
        $serviceManager = $e->getApplication()->getServiceManager();
        $config = $serviceManager->get('Config');
        
        $path = realpath(__DIR__.'/../../vendor/zendframework/zendframework/resources/languages/'.$config['translator']['locale']);
        $filename = 'Zend_Validate.php';

        $translator->addTranslationFile('phpArray', $path.DIRECTORY_SEPARATOR.$filename,null,$config['translator']['locale']);
        AbstractValidator::setDefaultTranslator($translator);
        
        $eventManager->attach(MvcEvent::EVENT_ROUTE, array(new Authentication(), 'checkAuthentication'));
        $eventManager->attach(MvcEvent::EVENT_ROUTE, array(new Acl(), 'checkPermissions'));
        
        $sessionManager = new SessionManager();
        $sessionManager->start();
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig()
    {
        return array(
        	'factories' => array(
        	   'Logger' => function($serviceManager){
        	       $logger = new Logger();
        	       $logger->addWriter(new Stream(
        	       		realpath(__DIR__ . '/../../data/log') .
        	            '/app.log'));
                    return $logger;
                }
        )
        );
    }
}
